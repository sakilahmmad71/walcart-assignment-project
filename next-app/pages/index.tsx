import { gql, useQuery } from '@apollo/client';
import type { NextPage } from 'next';
import styles from '../styles/Home.module.css';

const MENU_CATEGORIES = gql`{
	getCategories(
    pagination: {
      limit: 2
      skip: 0
    }
  )
  {
    message
    statusCode
    result {
      count
      categories {
        uid
        name
			}
		}
	}
}`;

const Home: NextPage = () => {
	const { loading, error, data } = useQuery(MENU_CATEGORIES);

	if (loading) {
		return <p>Loading...</p>;
	}

	if (error) {
		return <p>Error :(</p>;
	}

	return (
		<div className={styles.container}>
			<h1>Starting with nextjs</h1>
			{data && <p>{JSON.stringify(data, undefined, 2)}</p>}
		</div>
	);
};

export default Home;
