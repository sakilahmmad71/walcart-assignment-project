import React from 'react';

const BrancdIcon: React.FunctionComponent = () => {
	return (
		<img
			src="https://www.walcart.com/media/logo/stores/1/website_logo.png"
			className="w-32 md:w-32 lg:w-48 mx-2"
			alt="Walcart"
		/>
	);
};

export default BrancdIcon;
