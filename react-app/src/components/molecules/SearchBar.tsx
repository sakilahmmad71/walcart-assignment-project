import React from 'react';

const SearchBar: React.FunctionComponent = () => {
	return (
		<label htmlFor="search" className="relative block">
			<span className="sr-only">Search</span>
			<span className="absolute inset-y-0 left-0 flex items-center pl-2">
				<svg
					className="w-4 h-4 text-slate-400"
					fill="none"
					stroke="currentColor"
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
				>
					<path
						strokeLinecap="round"
						strokeLinejoin="round"
						strokeWidth="2"
						d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
					/>
				</svg>
			</span>
			<input
				className="py-2 placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-lg pl-9 pr-3 shadow-sm focus:outline-none focus:border-black focus:ring-1 sm:text-sm"
				placeholder="Search"
				type="text"
				name="search"
				id="search"
			/>
		</label>
	);
};

export default SearchBar;
