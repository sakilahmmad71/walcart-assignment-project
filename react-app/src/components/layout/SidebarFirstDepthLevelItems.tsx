/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useRef, useState } from 'react';
import { useHover } from 'usehooks-ts';
import SidebarSecondDepthLevelItems from './SidebarSecondDepthLevelItems';

const SidebarFirstDepthLevelItems: React.FunctionComponent = () => {
	const [showSubCategories, setShowSubCategories] = useState(false);
	const categoryItemRef = useRef<HTMLLIElement>(null);

	const isHovering = useHover(categoryItemRef);
	const toggleShowSubCategoris = () => setShowSubCategories(!showSubCategories);

	return (
		<>
			<li
				ref={categoryItemRef}
				className="relative hover:bg-white"
				onClick={toggleShowSubCategoris}
			>
				<a href="#home" className="flex justify-between items-center">
					<span>This is a category {isHovering && 'Hovering'}</span>
					<p>&gt;</p>
				</a>
			</li>

			{showSubCategories && <SidebarSecondDepthLevelItems />}
		</>
	);
};

export default SidebarFirstDepthLevelItems;
