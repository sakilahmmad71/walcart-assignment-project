/* eslint-disable no-param-reassign */
import React, { useRef } from 'react';
import { useOnClickOutside } from 'usehooks-ts';
import SidebarFirstDepthLevelItems from './SidebarFirstDepthLevelItems';

type SidebarPropsTypes = {
	state: Boolean;
	handler: (state: Boolean) => void;
};

// eslint-disable-next-line prettier/prettier
const Sidebar: React.FunctionComponent<SidebarPropsTypes> = ({ state = false, handler }) => {
	const menuRef = useRef<HTMLDivElement>(null);

	const handleToggleSidebarMenu = () => handler(!state);

	useOnClickOutside(menuRef, handleToggleSidebarMenu);

	// Testing purpose only
	const items = Array.from(Array(100).keys());

	return (
		<div
			ref={menuRef}
			className="w-80 bg-slate-100 rounded p-2 overflow-auto overscroll-contain scrollbar-hide"
			style={{ maxHeight: '88vh' }}
		>
			<ul>
				{items.map((item) => (
					<SidebarFirstDepthLevelItems key={item} />
				))}
			</ul>
		</div>
	);
};

export default Sidebar;
