import React, { useMemo, useState } from 'react';
import Account from '../../assets/svgs/Account';
import Help from '../../assets/svgs/Help';
import Notification from '../../assets/svgs/Notification';
import Settings from '../../assets/svgs/Settings';
import BrancdIcon from '../atoms/BrancdIcon';
import SearchBar from '../molecules/SearchBar';
import { HEADER_HEIGHT } from './constants';
import ExpandSidebarMenuIcon from './ExpandSidebarMenuIcon';
import Sidebar from './Sidebar';

const MenuIcons = [
	{
		id: '78ac6f04-dd32-11ec-9d64-0242ac120002',
		href: '/notifications',
		title: 'Notifications',
		component: <Notification />,
	},
	{
		id: '1fabcb71-3550-44f3-b3ed-727e8e881420',
		href: '/help',
		title: 'Help',
		component: <Help />,
	},
	{
		id: '15bd7da9-d207-4dbb-bdb1-45b3ae09edc1',
		href: '/settings',
		title: 'Settings',
		component: <Settings />,
	},
	{
		id: 'd43917f2-73db-4b22-a195-9c4bfd24526e',
		href: '/account',
		title: 'Account',
		component: <Account />,
	},
];

const Header: React.FunctionComponent = () => {
	const [openSidebarMenu, setOpenSidebarMenu] = useState<Boolean>(false);

	const { state, handler } = useMemo(
		() => ({ state: openSidebarMenu, handler: setOpenSidebarMenu }),
		[openSidebarMenu],
	);

	return (
		<nav
			data-testid="walcart-header"
			style={{ height: `${HEADER_HEIGHT}px` }}
			className="flex items-center justify-between bg-slate-50 p-4"
		>
			<div className="relative">
				<div className="flex items-center flex-shrink-0 text-stone-700 mr-6">
					<ExpandSidebarMenuIcon state={state} handler={handler} />
					<BrancdIcon />

					{openSidebarMenu && (
						<div className="absolute top-14">
							<Sidebar state={state} handler={handler} />
						</div>
					)}
				</div>
			</div>

			<div className="hidden justify-center align-middle items-center gap-4 lg:flex md:flex mx-4">
				<SearchBar />

				{MenuIcons.map((menuItem) => (
					<a
						key={menuItem.id}
						href={menuItem.href}
						className="text-sm rounded text-stone-500 hover:text-black hover:border-transparen"
						title={menuItem.title}
					>
						{menuItem.component}
					</a>
				))}
			</div>
		</nav>
	);
};

export default Header;
