/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useRef, useState } from 'react';
// import { useHover } from 'usehooks-ts';

const SidebarSecondDepthLevelItems: React.FunctionComponent = () => {
	const [showSubCategories, setShowSubCategories] = useState(false);
	const categoryItemRef = useRef<HTMLLIElement>(null);

	// const isHovering = useHover(categoryItemRef);
	const toggleShowSubCategoris = () => setShowSubCategories(!showSubCategories);

	const items = Array.from(Array(50).keys());

	return (
		<div
			className="absolute top-0 left-80 w-80 bg-slate-100 rounded p-2 overflow-auto overscroll-contain scrollbar-hide"
			style={{ maxHeight: '88vh' }}
		>
			<ul>
				{items.map((item) => (
					<li
						key={item}
						className="relative hover:bg-white"
						ref={categoryItemRef}
						onClick={toggleShowSubCategoris}
					>
						<a href="#home" className="flex justify-between items-center">
							<span>Second Categories Item</span>
							<p>&gt;</p>
						</a>
					</li>
				))}
			</ul>

			{/* {showSubCategories && <SidebarThirdDepthLevelItems />} */}
		</div>
	);
};

export default SidebarSecondDepthLevelItems;
