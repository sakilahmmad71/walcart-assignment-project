import React from 'react';
import { FOOTER_HEIGHT, HEADER_HEIGHT } from './constants';
import Footer from './Footer';
import Header from './Header';

const AppLayout: React.FunctionComponent = () => {
	return (
		<>
			<Header />
			{/* eslint-disable-next-line prettier/prettier */}
			<main style={{ minHeight: `calc(100vh - ${HEADER_HEIGHT + FOOTER_HEIGHT}px)`}} />
			<Footer />
		</>
	);
};

export default AppLayout;
