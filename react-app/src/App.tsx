import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import React from 'react';
import BaseLayout from './components/layout/BaseLayout';

const client = new ApolloClient({
	uri: 'https://devapiv2.walcart.com/graphql',
	cache: new InMemoryCache(),
});

const App: React.FunctionComponent = () => {
	return (
		<ApolloProvider client={client}>
			<div>
				<BaseLayout />
			</div>
		</ApolloProvider>
	);
};

export default App;
